
import  Layout  from '../components/Layout'
import { 
  Route, 
  BrowserRouter as Router, 
  Switch, 
  Redirect
} from 'react-router-dom'
import Options from './../pages/Options'
import { LINK_OPTIONS } from './../utils/links'

const Routes = () => {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path={LINK_OPTIONS} component= {Options}/>
          <Redirect to={LINK_OPTIONS}/>
        </Switch>
      </Layout>
    </Router>
  )
}

export default Routes