import { Link } from "react-router-dom";
import styled from "styled-components"
import { data } from "./data";

const Container = styled.div`
  position: sticky;
  top: 0;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  height: 80px;
  box-shadow: 0px 0px 10px 4px rgb(0 0 0 / 9%);
  background: #0b304a;
  font-weight: bold;
`;

const Header = () => {
  return (
    <Container>
      {data?.map((menu : any, key) => {
        return (
        <Link  style={{ color: '#fff'}} key={key} to={menu?.link}>{menu?.name}</Link>
        )
      })}
    </Container>
  )
}

export default Header