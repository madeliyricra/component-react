import { LINK_OPTIONS } from "../../utils/links"

export const data = [
  {
    name: 'Options',
    link : LINK_OPTIONS
  }
]