import  Header from "../Header";

interface IProps {
  children?: JSX.Element,
}

const Layout = ({children} : IProps) => {
  console.log(children);
  return (
    <div>
      <Header />
      {children}
    </div>
  )
}

export default Layout