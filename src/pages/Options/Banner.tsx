import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 15px;
  padding: 20px;
  width: 350px;
  height: 150px;
  border-radius: 10px;
  background-color: #fff0ea;
`;

const Title = styled.h2`
  width: 300px;
  font-size: 16px;
  color: #ff8b60;
  text-align: center;
  text-transform: uppercase;
`;

const Button = styled.button`
  padding: 7px 25px;
  background-color: #f06737;
  opacity: 0.9;
  font-size: 13px;
  font-weight: bold;
  text-transform: uppercase;
  color: #fff;
  border-radius: 25px;
  cursor: pointer;
`;

const Banner = () => {
  return (
    <Container>
      <Title>Use code: pigi100 for rs.100 off on your first order! </Title>
      <Button>Grab now</Button>
    </Container>
  );
};

export default Banner;
