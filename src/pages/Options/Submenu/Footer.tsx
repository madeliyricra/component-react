import styled from "styled-components";
import save from "../../../assets/icons/save.png";
import reload from "../../../assets/icons/reload.png";
import close from "../../../assets/icons/close.png";

const ContainerFloating = styled.div`
  position: absolute;
  left: 0;
  bottom: -20px;
  display: flex;
  justify-content: center;
  gap: 10px;
  z-index: 1000;
  width: 100%;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  padding: 15px 10px 0;
  margin: 15px 0 0 0;
  border-top: 2px solid #ffffff80;
`;

const Icon = styled.img`
  width: 40px;
  height: 40px;
  padding: 5px;
  background-color: #fff;
  object-fit: override;
  cursor: pointer;
  border-radius: 50%;
  box-shadow: 1px 2px 8px 0px #979797;
`;

export interface IFooter {
  version:string,
  onReset?: () => void,
  onCancel?: () => void,
  onSave?: () => void,
}

interface IOption {
  icon : string,
  handleClick : () => void,
}

const Footer = ({version, onReset, onCancel, onSave} : IFooter) => {
  const handleReset = () => {
    if(typeof onReset === 'function') onReset();
  }

  const handleSave = () => {
    if(typeof onSave === 'function') onSave();
  }

  const handleCancel = () => {
    if(typeof onCancel === 'function') onCancel();
  }

  switch(version){
    case "floating":
      const dataFloating : IOption[] = [
        {
          icon: close,
          handleClick: ()=>{}
        },
        {
          icon: save,
          handleClick: ()=>{}
        }
      ]
      return (
        <ContainerFloating>
          {dataFloating?.map((icon, key) => {
            return <Icon key={key} src={icon?.icon} onClick={icon?.handleClick}/>
          })}
        </ContainerFloating>
      );
    case "normal":
      const data : IOption[]= [
        {
          icon: close,
          handleClick: handleCancel
        },
        {
          icon: reload,
          handleClick: handleReset
        },
        {
          icon: save,
          handleClick: handleSave
        }
      ]
      return (
        <Container> 
          {data?.map((icon, key) => {
            return <Icon key={key} src={icon?.icon} onClick={icon?.handleClick}/>
          })}
        </Container>
      );
    default:
      return <></>;
  }
}

export default Footer