import styled from "styled-components";
import Checkbox from "./Checkbox";
import check from "../../../assets/icons/check.png";

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px;
`;

const Title = styled.h5`
  color: #fff;
  font-weight: 500;
  font-size: 17px;
  text-transform: uppercase;
`;

export interface IOption{
  title: string,
  onChange?: (state: boolean, value: string) => void;
}

const Option = ({ title, onChange }: IOption) => {
  const handleChange = (state: boolean) => {
    if(typeof onChange === 'function') onChange(state, title);
  };

  return (
    <Container>
      <Title>{title}</Title>
      <Checkbox onChange={handleChange} icon= {check}/>
    </Container>
  );
};

export default Option;
