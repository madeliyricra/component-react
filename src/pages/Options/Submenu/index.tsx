import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Footer from "./Footer";
import Option, { IOption } from "./Option";

export interface ISubmenu{
  color : string, 
  options : string[], 
  footer: string, 
  onChange?: (state: boolean, value : string) => void;
}

const Container = styled.div<{version:string}>`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-bottom: 20px;
  padding: ${(props) => props.version === 'floating' ?  '30px 20px 50px': '30px 20px'};
  width: 350px;
  background-color: ${(props) => props.color};
  border-radius: 25px;
  opacity: 0.9;
`;

const Submenu = ({color, options, footer, onChange}:ISubmenu )=> {
  const handleChange = (state: boolean, value : string) =>{
    if(typeof onChange === 'function') onChange(state, value);
  }

  return (
    <Container color={color} version={footer}>
      {options?.map((option : string, key) => <Option title={option} key={key} onChange ={handleChange} />)}
      <Footer version={footer}/>
    </Container>
  );
};

export default Submenu;
