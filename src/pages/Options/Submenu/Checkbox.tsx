import { useState } from 'react';
import styled from 'styled-components';

const Container = styled.div<{visible: boolean}>`
  width: 32px;
  height: 32px;
  background-color: ${(props) => (props.visible ? "#fff" : "transparent")};
  border-radius: 50%;
  border: 2px solid #fff;
  cursor: pointer;
`;

const Icon = styled.img<{visible: boolean}>`
  display: ${(props) => (props.visible ? "visible" : "none")};
  width: 30px;
  height: 30px;
  object-fit: override;
`;

interface ICheckbox{
  icon: string,
  onChange?: (value: boolean) => void
}

const Checkbox = ({ icon, onChange} : ICheckbox) => {
  const [visible, setVisible] = useState(false)

  const handleChange = () => {
    setVisible(!visible)
    if(typeof onChange === 'function') onChange(!visible)
  };

  return (
    <Container visible={visible} onClick={handleChange}>
      <Icon src={icon} visible={visible} />
    </Container>
  )
}

export default Checkbox