import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Banner from "./Banner";
import Submenu, { ISubmenu } from "./Submenu";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 10px;
  padding: 20px;
`;

const Options = () => {
  const data = [
    {
      color: "#21d0d0",
      options: [
        "Price low to high",
        "Price high to low",
        "Popularity",
      ],
      footer: "floating"
    },
    {
      color: "#ff7745",
      options:[
        "1Up nutririon",
        "Asitis",
        "Avvatar",
        "Big muscles",
        "BPI sports",
        "BSN",
      ],
      footer: "normal"
    }
  ]

  const handleChange = (state: boolean, value : string) =>{
    console.log(value, state)
  }

  return (
    <Container>
      <Banner />
      {data.map((submenu : ISubmenu, key) => {
        return <Submenu 
        key={key} 
        {...submenu} 
        onChange= {handleChange}/>
      })}
    </Container>
  );
};

export default Options;
